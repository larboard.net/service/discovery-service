package net.larboard.service.discovery.authentication.security;

import net.larboard.lib.auth.security.HeaderTokenAuthenticationProvider;
import net.larboard.lib.auth.service.HeaderTokenAuthenticationService;
import org.springframework.stereotype.Component;

@Component
public class DiscoveryHeaderTokenAuthenticationProvider extends HeaderTokenAuthenticationProvider {
    public DiscoveryHeaderTokenAuthenticationProvider(HeaderTokenAuthenticationService authenticationService) {
        super(authenticationService);
    }
}