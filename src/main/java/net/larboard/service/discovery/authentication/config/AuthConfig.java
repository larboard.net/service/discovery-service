package net.larboard.service.discovery.authentication.config;

import lombok.Getter;
import lombok.Setter;
import net.larboard.lib.auth.provider.jwt.IJwtAuthConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Validated
@Configuration
@ConfigurationProperties("application.authentication")
@Getter
@Setter
public class AuthConfig implements IJwtAuthConfig {
    @NotNull
    private String jwtKeyHost;

    @NotNull
    private String jwtKeyUrl;

    @NotNull
    private String jwtIssuer;
}