package net.larboard.service.discovery.authentication.service;

import com.netflix.discovery.EurekaClient;
import io.jsonwebtoken.Claims;
import net.larboard.lib.auth.provider.jwt.AJwtAuthenticationService;
import net.larboard.service.discovery.authentication.config.AuthConfig;
import net.larboard.service.discovery.authentication.data.AuthUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class DiscoveryJwtAuthenticationService extends AJwtAuthenticationService {
    @Autowired
    public DiscoveryJwtAuthenticationService(AuthConfig authConfig, RestTemplate restTemplate, @Qualifier("eurekaClient") EurekaClient eurekaClient) {
        super(authConfig, restTemplate, eurekaClient);
    }

    @Override
    protected User processUser(Claims claims, String headerToken) {
        return new AuthUser(
                claims.getIssuer(),
                headerToken,
                Collections.singleton(new SimpleGrantedAuthority("ROLE_SYSTEM")
                )
        );
    }
}
